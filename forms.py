#encoding: utf-8
'''
@Author：Steward 
@Time :12/10/2019 3:55 AM
@File :forms.PY
'''

# from flask_wtf import Form
from wtforms import StringField, IntegerField, TextAreaField, SubmitField, RadioField, SelectField, FloatField
from wtforms import Form, BooleanField,  PasswordField, validators
from wtforms import validators, ValidationError

class ContactForm(Form):
    name = StringField("学生姓名",[validators.data_required("Please enter your name.")])
    Gender = RadioField('性别', choices = [('M','Male'),('F','Female')])
    Address = TextAreaField("地址")

    email = StringField("Email",[validators.required("Please enter your email address."),
      validators.Email("Please enter your valid email address.")])

    Age = IntegerField("年龄")
    language = SelectField('语言', choices = [('cpp', 'C++'), ('py', 'Python')])
    submit = SubmitField("提交")



class RegistrationForm(Form):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email Address', [validators.Length(min=6, max=35),
                                          validators.Email("Please enter your valid email address.")])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the TOS', [validators.DataRequired()])


class CurveForm(Form):
    a = FloatField('A', [validators.number_range(min=-5.0, max=5.0)])
    b = FloatField('B', [validators.number_range(min=-5.0, max=5.0)])
    c = FloatField('C', [validators.number_range(min=-5.0, max=5.0)])
