#encoding: utf-8
'''
@Author：Steward 
@Time :12/3/2019 2:29 PM
@File :multifunc.PY
'''

from flask import Flask, g, render_template
import flask_sijax #import sijax

# path = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')
app = Flask(__name__)

# app.config['SIJAX_STATIC_PATH'] = path
# app.config['SIJAX_JSON_URI'] = '/static/js/sijax/json2.js'
instance = flask_sijax.Sijax(app)

class Handler(object):

    @staticmethod
    def say_hi(obj_response):
        obj_response.alert("Hifff!")

    @staticmethod
    def say_hello(obj_response):
        obj_response.alert("Hellofff!")

    @staticmethod
    def change_color(obj_response,data):
        obj_response.css('#show','color', 'red')
        print(data)

# sijax_instance.register_object(Handler)

@app.route('/')
def index():
   return 'Index'

@flask_sijax.route(app, '/hello/')
def hello():
    # def say_hi(obj_response):
    #     obj_response.alert('Hi there!')
    #     obj_response.attr('#show', 'align','center')
    #     obj_response.attr('#show', 'innerHTML', 'right')
    # def say_bye(obj_response):
    #     obj_response.alert('Say bye!')
    #     data = "say bye"
    if g.sijax.is_sijax_request:
        # Sijax request detected - let Sijax handle it
        # g.sijax.register_callback('say_hi', say_hi)
        # g.sijax.register_callback('say_bye', say_bye)
        instance.register_object(Handler)
        return g.sijax.process_request()
    return render_template('sijaxexample.html')

if __name__ == '__main__':
    app.run(debug = True)

