#encoding: utf-8
'''
@Author：Steward 
@Time :12/3/2019 10:28 AM
@File :myapp.PY
'''

import os
from flask import Flask, g, render_template
import flask_sijax #import sijax

# path = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')
app = Flask(__name__)

# app.config['SIJAX_STATIC_PATH'] = path
# app.config['SIJAX_JSON_URI'] = '/static/js/sijax/json2.js'
flask_sijax.Sijax(app)

@app.route('/')
def index():
   return 'Index'

@flask_sijax.route(app, '/hello/')
def hello():
    hello_from = 'test'
    def say_hi(obj_response,hello_from):
        obj_response.alert(hello_from)
        # obj_response.attr('#show', 'align','center')
        # obj_response.attr('#show', 'innerHTML', 'right')
    def say_bye(obj_response):
        obj_response.alert('Say bye!')
        data = "say bye"
    if g.sijax.is_sijax_request:
        # Sijax request detected - let Sijax handle it
        g.sijax.register_callback('say_hi', say_hi, args_extra=[hello_from])
        g.sijax.register_callback('say_bye', say_bye)
        return g.sijax.process_request()
    return render_template('sijaxexample.html')

if __name__ == '__main__':
    app.run(debug = True)

