#encoding: utf-8
'''
@Author：Steward
@Time :12/10/2019 4:06 AM
@File :testformwtf.PY
'''

from flask import Flask, render_template, request, flash, url_for
from forms import ContactForm, RegistrationForm, CurveForm
import flask_sijax

import matplotlib.pyplot as plt
import numpy as np

import base64
from io import BytesIO
from math import pi, cos, sin

from matplotlib.patches import PathPatch
from matplotlib.path import Path
from mpl_toolkits.axisartist.axislines import SubplotZero
from mpl_toolkits.mplot3d import Axes3D

app = Flask(__name__)
app.secret_key = 'development key'
flask_sijax.Sijax(app)

# format axis add x,y axis arrows


def formatAxis():
    fig = plt.figure()
    ax = SubplotZero(fig, 111)
    fig.add_subplot(ax)
    ax.axis('equal')

    for direction in ["xzero", "yzero"]:
        # adds arrows at the ends of each axis
        ax.axis[direction].set_axisline_style("-|>")

        # adds X and Y-axis from the origin
        ax.axis[direction].set_visible(True)

    for direction in ["left", "right", "bottom", "top"]:
        # hides borders
        ax.axis[direction].set_visible(False)

    return fig, ax

# convert imgage data


def FormatImgdata(plt):
    # 转成图片的步骤
    sio = BytesIO()
    plt.savefig(sio, format='png')
    data = base64.encodebytes(sio.getvalue()).decode()
    src = 'data:image/png;base64,' + str(data)
    sio = ''

    # 记得关闭，不然画出来的图是重复的
    plt.close()
    return src


@app.route('/')
def index():
    return render_template('base.html')


@app.route('/curve/', methods=['GET', 'POST'])
def curve():
    form = CurveForm(request.form)
    if request.method == 'POST' and form.validate():
        a = form.a.data
        b = form.b.data
        c = form.c.data
        print(type(a))

        if a == 0:
            if b == 0:
                expression = '\\(y = ' + str(c) + '\\)'
            elif c == 0:
                expression = '\\(y = ' + str(b) + 'x \\)'
            elif c < 0:
                expression = '\\(y = ' + str(b) + 'x ' + str(c) + '\\)'
            else:
                expression = '\\(y = ' + str(b) + 'x +' + str(c) + '\\)'
        else:
            if b == 0:
                if c < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(c) + '\\)'
                elif c == 0:
                    expression = '\\(y = ' + str(a) + 'x^2 \\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(c) + '\\)'
            elif c == 0:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(b) + 'x \\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x \\)'
            elif c < 0:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + str(b) + 'x ' + str(c) + '\\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x ' + str(c) + '\\)'
            else:
                if b < 0:
                    expression = '\\(y = ' + str(a) + 'x^2 ' + \
                                 str(b) + 'x +' + str(c) + '\\)'
                else:
                    expression = '\\(y = ' + str(a) + 'x^2 +' + \
                    str(b) + 'x +' + str(c) + '\\)'
        # expression = '\\(y = ' + str(a) + 'x^2 +' + str(b) + 'x +' + str(c) + '\\)'
        print(form.a.data, form.b.data, form.c.data)
        flash('Thanks for drawing curve: '+ expression)
        x = np.linspace(-10, 10, 200)
        y = (a * x * x) + (b * x) + c

        formatAxis()  # figsize=(8, 5))
        plt.plot(x, y)

        plt.tight_layout()

        src = FormatImgdata(plt)
        # plt.close()
        # 记得关闭，不然画出来的图是重复的

        params = {
            'form': form,
            'expression': expression,
            'src': src,
        }
        return render_template('curve.html', **params)

    return render_template('curve.html', form=form)


@app.route('/testsijax/')
def testsijax():
    return render_template('testsijax.html')


@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    form = ContactForm()

    if request.method == 'POST':
        if form.validate() == False:
            flash('All fields are required.')
            return render_template('contact.html', form=form)
        else:
            return render_template('success.html')
    elif request.method == 'GET':
        return render_template('contact.html', form=form)


@app.route('/register/', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        # user = User(form.username.data, form.email.data,
        #             form.password.data)
        # db_session.add(user)
        print(form.username.data, form.email.data, form.password.data)
        flash('Thanks for registering')
        # return redirect(url_for('login'))
        return render_template('base.html')
    return render_template('register.html', form=form)


if __name__ == '__main__':
    app.run(debug=True)
